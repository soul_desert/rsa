package edu.my;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Alex on 28.11.2016.
 */
public class Receiver {

    private BigInteger p, q, n, k;
    private Double d;
    private int e;


    public Receiver() {
        p = BigInteger.probablePrime(16, new Random());
        q = BigInteger.probablePrime(16, new Random());
    }

    public void generateE() {
        e = 65537;                  // простое число Ферма
    }

    public void generateN() {
        n = p.multiply(q);
        generateK();
    }

    private void generateK() {
        BigInteger p_minus = p.subtract(BigInteger.ONE);
        BigInteger q_minus = q.subtract(BigInteger.ONE);
        k = p_minus.multiply(q_minus);
    }

    public void generateD() {

        BigInteger mult = k.add(BigInteger.ONE);
        while(!(mult.remainder(BigInteger.valueOf(e))).equals(BigInteger.ZERO)) {
            mult = mult.add(k);
        }
        d = mult.divide(BigInteger.valueOf(e)).doubleValue();
    }

    public int getE() {
        return e;
    }

    public BigInteger getN() {
        return n;
    }

    public String decrypt(List<String> message) {

        char[] abc = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'N', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        List<Character> Alphabet = new ArrayList<>();

        for (char ch : abc) {
            Alphabet.add(ch);
        }

        String decrypted = "";

        for(String symbol : message) {
            try  {                                                             // проверяем каждый из элементов сообщения
                BigInteger c = new BigInteger(symbol);                         // если symbol - число, то декодируем его
                BigInteger m = c.modPow(BigInteger.valueOf(d.longValue()), n);      // и добавляем к тексту сообщения расшифрованный символ
                decrypted += Alphabet.get(m.intValue()).toString();
            } catch (NumberFormatException e) {
                decrypted += symbol;                                      // если не число, то это просто символ, не
            }                                                             // нуждающийся в расшифровке
        }

        return decrypted;

    }

}
