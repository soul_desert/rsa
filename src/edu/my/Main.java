package edu.my;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        Sender sender = new Sender();
        Receiver receiver = new Receiver();

        receiver.generateE();                   //  генерация ключей
        receiver.generateN();                   //
        receiver.generateD();                   //  получателем

        sender.getPublicKey(receiver.getE(), receiver.getN());      //  ключ сообщается отправителю
        String decrypted;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("Decrypted.txt"))){
            decrypted = receiver.decrypt(sender.encrypt());         //  получатель расшифровывает пересланное сообщение
            writer.write(decrypted);                                //  (из файла Text.txt)
            writer.close();                                         //  и записывает в Decrypted.txt
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
