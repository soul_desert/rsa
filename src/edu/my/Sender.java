package edu.my;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 28.11.2016.
 */
public class Sender {

    private int e;
    private BigInteger n;

    public void getPublicKey(int e, BigInteger n) {
        this.e = e;
        this.n = n;
    }

    public List<String> encrypt() throws IOException {

        char[] abc = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'N', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        List<Character> Alphabet = new ArrayList<>(); //new ArrayList<>(Arrays.asList(abc));

        List<String> encryptedMessage = new ArrayList<>();

        for (char ch : abc) {
            Alphabet.add(ch);
        }
        String message = readFile();                            // файл считывается в строку

        for (char ch : message.toCharArray()) {                 // для каждого символа строки
            if (Alphabet.contains(ch)) {                        // если он буква
                int m = Alphabet.indexOf(ch);                   // то вычисляем порядковый номер этого символа в алфавите
                BigInteger c = BigInteger.valueOf(m).modPow(BigInteger.valueOf(e), n);  // и кодируем его
                encryptedMessage.add(c.toString());             // добавляем зашифрованный символ в пересылаемое сообщение
            } else {
                encryptedMessage.add(String.valueOf(ch));       // если не буква, то символ передается в сообщение без шифрования
            }
        }

        return encryptedMessage;

    }

    private String readFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("Text.txt"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

}
